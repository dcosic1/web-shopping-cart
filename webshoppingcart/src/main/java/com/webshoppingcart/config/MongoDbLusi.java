
package com.webshoppingcart.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

/*
@Configuration
@ComponentScan(basePackages = "com.webshoppingcart.service")
@EnableMongoRepositories(basePackages = "com.webshoppingcart.repository")
@EnableTransactionManagement*/
public class MongoDbLusi {
	
	@Bean 
	public MongoClient mongo() {
		MongoClientURI url = new MongoClientURI("mongodb://dbUser:password29@cluster0-shard-00-00-xusva.mongodb.net:27017,cluster0-shard-00-01-xusva.mongodb.net:27017,cluster0-shard-00-02-xusva.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true");
		return new MongoClient(url);
	}
	
	@Bean 
	public MongoTemplate mongoTemplate() throws Exception {
		return new MongoTemplate(mongo(), "projekat");
	}
}
