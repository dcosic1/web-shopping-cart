

package com.webshoppingcart.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.webshoppingcart.model.MongoAccount;
import com.webshoppingcart.service.MongoAccountService;

@RestController
public class MongoAccountController {
	
	@Autowired
	MongoAccountService mongoAccountService;

	@GetMapping("/mongoAccounts")
	public Iterable<MongoAccount> getAccounts() {
		return mongoAccountService.getAccounts();
	}
	
	@GetMapping("/mongoAccount")
	public Optional<MongoAccount> getProduct(@RequestParam(value="id") String id) {
		return mongoAccountService.getAccountById(id);
	}
}
