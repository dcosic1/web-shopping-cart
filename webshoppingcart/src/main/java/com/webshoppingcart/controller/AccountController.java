package com.webshoppingcart.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.webshoppingcart.model.Account;
import com.webshoppingcart.service.AccountService;

@RestController
public class AccountController {
	@Autowired
	AccountService accountService;

	@GetMapping("/accountsAll")
	public Iterable<Account> getAccounts() {
		return accountService.getAccounts();
	}
	
	@GetMapping("/account")
	public Optional<Account> getAccount(@RequestParam(value="id") Integer id) {
		return accountService.getAccountById(id);
	}
}
