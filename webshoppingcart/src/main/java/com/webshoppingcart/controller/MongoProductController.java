package com.webshoppingcart.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.webshoppingcart.model.MongoProduct;
import com.webshoppingcart.service.MongoProductService;

@RestController
public class MongoProductController {
	
	@Autowired
	MongoProductService mongoProductService;
	/**@param Rutiranje za testiranje Mongo baze podataka */
	@GetMapping("/mongoProducts")
	public Iterable<MongoProduct> getProducts() {
		return mongoProductService.getProducts();
	}
	
	@GetMapping("/mongoProduct")
	public Optional<MongoProduct> getProduct(@RequestParam(value="id") String id) {
		return mongoProductService.getProductById(id);
	}
}
