package com.webshoppingcart.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.webshoppingcart.model.Order;
import com.webshoppingcart.service.OrderService;

@RestController
public class OrderController {
	@Autowired
	OrderService orderService;

	@GetMapping("/ordersAll")
	public Iterable<Order> getOrders() {
		return orderService.getOrders();
	}
	
	@GetMapping("/order")
	public Optional<Order> getOrder(@RequestParam(value="id") Integer id) {
		return orderService.getOrderById(id);
	}
}
