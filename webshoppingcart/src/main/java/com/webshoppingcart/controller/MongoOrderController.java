package com.webshoppingcart.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.webshoppingcart.model.MongoOrders;
import com.webshoppingcart.service.MongoOrderService;


@RestController
public class MongoOrderController {

	@Autowired
	MongoOrderService mongoOrdersService;
	@GetMapping("/mongoOrders")
	public Iterable<MongoOrders> getOrders() {
		return mongoOrdersService.getOrders();
	}
	
	@GetMapping("/mongoOrder")
	public Optional<MongoOrders> getOrder(@RequestParam(value="id") String id) {
		return mongoOrdersService.getOrderById(id);
	}
}
