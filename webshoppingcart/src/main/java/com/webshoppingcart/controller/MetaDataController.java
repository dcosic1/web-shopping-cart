package com.webshoppingcart.controller;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.sql.Savepoint;
import com.webshoppingcart.connection.Database;
import com.webshoppingcart.connection.DatabasteOracle;
import com.webshoppingcart.model.Column;
import com.webshoppingcart.model.PrimaryKey;
import com.webshoppingcart.model.Procedures;
import com.webshoppingcart.model.Table;
import com.webshoppingcart.service.MetaDataService;

import com.webshoppingcart.service.MigrationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class MetaDataController {
    @Autowired
    MetaDataService metaDataService;
   
    @Autowired
    MigrationService migracija;

    @RequestMapping(value="/metadata", method=RequestMethod.GET)
    public String getMetaData(Model model) {
    	Connection con=null;
    	Savepoint svpt1=null;
    	try {
        List<Table> tables = metaDataService.getTables();
        List<Procedures> procedures = metaDataService.getProcedure();
        List<Procedures> functions = metaDataService.getFunctions();
        List<String> trigers = metaDataService.getTrigers();
       
        
        model.addAttribute("tables", tables);
        model.addAttribute("procedures", procedures);
        model.addAttribute("functions", functions);
        model.addAttribute("trigers", trigers);
        
       
    	con=Database.getConnection();
        
        Statement statment=con.createStatement();
        
        Connection konekcija=DatabasteOracle.getConnection();
        
        con.setAutoCommit(false);
        Statement stmt2=con.createStatement();
        stmt2.execute("SET @@global.autocommit= 1;");
       
        migracija.migracijaTabela(tables,con);
        
        stmt2.execute("SET @@global.autocommit= 0;");
        svpt1=con.setSavepoint("SavePoint1");
        stmt2.execute("START TRANSACTION;");
        migracija.migracijaPodataka(tables,con, konekcija);
        con.commit();
        //stmt2.execute("commit;");
	}catch(Exception e) {
		if(con!=null) {
			try {
				Statement stmt2=con.createStatement();
        		con.rollback(svpt1);
        		System.out.println("ovdje bi trebalo da uhvati baceni throw");
			}catch(Exception e1) {
				
			}
			
		}
		
		
    	e.printStackTrace();
    }
    

    return "metadata";
}

}