package com.webshoppingcart.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.webshoppingcart.model.Product;
import com.webshoppingcart.service.ProductService;

@RestController
public class ProductController {

	@Autowired
	ProductService productService;
	/**@param Rutiranje za testiranje baze podataka */
	@GetMapping("/productsAll")
	public Iterable<Product> getProducts() {
		return productService.getProducts();
	}
	
	@GetMapping("/product")
	public Optional<Product> getProduct(@RequestParam(value="id") Integer id) {
		return productService.getProductById(id);
	}
}
