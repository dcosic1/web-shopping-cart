package com.webshoppingcart.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.webshoppingcart.forms.UploadForm;
import com.webshppingcart.servlets.UploadToDBServlet;

@Controller
public class FileUploadController {
	@RequestMapping(value = "/")
	public String homePage() {

		return "index";
	}

	// GET: Show upload form page.
	@RequestMapping(value = "/uploadOneFile", method = RequestMethod.GET)
	public String uploadOneFileHandler(Model model) {

		UploadForm myUploadForm = new UploadForm();
		model.addAttribute("myUploadForm", myUploadForm);

		return "uploadOneFile";
	}

	// POST: Do Upload
	@RequestMapping(value = "/uploadOneFile", method = RequestMethod.POST)
	public String uploadOneFileHandlerPOST(HttpServletRequest request, //
			Model model, //
			@ModelAttribute("myUploadForm") UploadForm myUploadForm) {

		return this.doUpload(request, model, myUploadForm);

	}

	// GET: Show upload form page.
	@RequestMapping(value = "/uploadMultiFile", method = RequestMethod.GET)
	public String uploadMultiFileHandler(Model model) {

		UploadForm myUploadForm = new UploadForm();
		model.addAttribute("myUploadForm", myUploadForm);

		return "uploadMultiFile";
	}

	// POST: Do Upload
	@RequestMapping(value = "/uploadMultiFile", method = RequestMethod.POST)
	public String uploadMultiFileHandlerPOST(HttpServletRequest request, //
			Model model, //
			@ModelAttribute("myUploadForm") UploadForm myUploadForm) {

		return this.doUpload(request, model, myUploadForm);

	}

	private String doUpload(HttpServletRequest request, Model model, //
			UploadForm myUploadForm) {

		String description = myUploadForm.getDescription();
		System.out.println("Description: " + description);

		// Root Directory.
		String uploadRootPath = request.getServletContext().getRealPath("upload");
		System.out.println("uploadRootPath=" + uploadRootPath);

		File uploadRootDir = new File(uploadRootPath);
		UploadToDBServlet servlet = new UploadToDBServlet();
		Connection conn = null;
		try {
			
			String hostName = "ora.db.lab.ri.etf.unsa.ba";
			String sid = "ETFLAB";
			String userName = "BP21";
			String password = "wP78eev7";

			String connectionURL = "jdbc:oracle:thin:@" + hostName + ":1521:" + sid;

			conn = DriverManager.getConnection(connectionURL, userName, password);
			conn.setAutoCommit(false);

			// LOKALNO
			// Create directory if it not exists.
			if (!uploadRootDir.exists()) {
				uploadRootDir.mkdirs();
			}
			MultipartFile[] fileDatas = myUploadForm.getFileDatas();
			//
			List<File> uploadedFiles = new ArrayList<File>();
			List<String> failedFiles = new ArrayList<String>();

			for (MultipartFile fileData : fileDatas) {

				// Client File Name
				String name = fileData.getOriginalFilename();
				System.out.println("Client File Name = " + name);

				if (name != null && name.length() > 0) {
					try {
						// Create the file at server
						File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);

						BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
						stream.write(fileData.getBytes());
						stream.close();
						//

						uploadedFiles.add(serverFile);
						servlet.writeToDB(conn, name, fileData.getInputStream(), description);
						System.out.println("Write file: " + serverFile);
					} catch (Exception e) {
						System.out.println("Error Write file: " + name);
						failedFiles.add(name);
					}
				}
			}
			model.addAttribute("description", description);
			model.addAttribute("uploadedFiles", uploadedFiles);
			model.addAttribute("failedFiles", failedFiles);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closeQuietly(conn);
		}
		return "uploadResult";
	}

	private void closeQuietly(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
		}
	}
}
