package com.webshoppingcart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.webshoppingcart.repository.MongoProductRepository;
import com.webshoppingcart.service.AccountService;
import com.webshoppingcart.service.MongoProductService;
import com.webshoppingcart.service.OrderService;
import com.webshoppingcart.service.ProductService;


@SpringBootApplication
public class Application {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	OrderService orderService;
	
	// MongoDb
	//@Autowired
	//MongoProductService mongoProductService;
	
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
