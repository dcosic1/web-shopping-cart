package com.webshoppingcart.model;


public class PrimaryKey {
    String column;
    String name;

    public PrimaryKey(String column, String name) {
        this.column = column;
        this.name = name;
    }

    public String getColumn() {
        return column;
    }

    public String getName() {
        return name;
    }
}