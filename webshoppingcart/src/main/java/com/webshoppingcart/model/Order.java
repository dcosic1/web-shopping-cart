package com.webshoppingcart.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ORDERS")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_Sequence")
	@SequenceGenerator(name = "order_Sequence", sequenceName = "ORDER_SEQ")
	private Integer id;

	@Column(name = "amount")
	private double amount;

	@Column(name = "customer_name")
	private String customer_name;
	
	@Column(name = "customer_adress")
	private String customer_adress;
	
	@Column(name = "customer_email")
	private String customer_email;
	
	@Column(name = "customer_phone")
	private String customer_phone;

	@Column(name = "order_date")
	private Date order_date;
	
	@Column(name = "order_num")
	private Integer order_num;

	

	public Order() {
	}

	// getters/setters
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customer_name;
	}

	public void setCustomerName(String customer_name) {
		this.customer_name = customer_name;
	}
	
	public String getCustomerEmail() {
		return customer_email;
	}

	public void setCustomerEmail(String customer_email) {
		this.customer_email = customer_email;
	}

	public String getCustomerAdress() {
		return customer_adress;
	}

	public void setCustomerAdress(String customer_adress) {
		this.customer_adress = customer_adress;
	}
	
	public String getCustomerPhone() {
		return customer_phone;
	}

	public void setCustomerPhone(String customer_phone) {
		this.customer_phone = customer_phone;
	}
	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDateOrder() {
		return order_date;
	}

	public void setDateOrder(Date date_created) {
		this.order_date = date_created;
	}
	
	public Integer getOrderNum() {
		return order_num;
	}

	public void setOrderNum(Integer id) {
		this.order_num = id;
	}

	
}
