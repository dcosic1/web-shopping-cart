package com.webshoppingcart.model;

public class Indeks {
    String name;
    short type;
    String column;

    public Indeks(String name, short type, String column) {
        this.name = name;
        this.type = type;
        this.column = column;
    }

    public String getName() {
        return name;
    }

    public short getType() {
        return type;
    }

    public String getStringType() {
        switch(type) {
            case 1:
                return "tableIndexStatistic";
            case 2:
                return "tableIndexClustered";
            case 3:
                return "tableIndexHashed";
            case 4:
                return "tableIndexOther";
            default:
                return "";
        }
    }

    public String getColumn() {
        return column;
    }
}