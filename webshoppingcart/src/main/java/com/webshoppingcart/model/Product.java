package com.webshoppingcart.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/** @author Dino Cosic */

/**@deprecated  Klasa Products predstavlja entitet iz baze podataka. Sastoji se od osnovnih atributa koji opisuju proizvod koji se nalazi u bazi*/
@Entity
@Table(name = "PRODUCTS")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_Sequence")
	@SequenceGenerator(name = "product_Sequence", sequenceName = "PRODUCT_SEQ")
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "price")
	private double price;

	@Column(name = "date_created")
	private Date date_created;
	
	@Lob
	@Column(name = "image", columnDefinition="BLOB")
	private byte[] image;

	// Foreign key
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "team_id", nullable = false)
//	private Team team;

	public Product() {
	}

	// getters/setters
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getDate_created() {
		return date_created;
	}

	public void setDate_created(Date date_created) {
		this.date_created = date_created;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
	
	
}
