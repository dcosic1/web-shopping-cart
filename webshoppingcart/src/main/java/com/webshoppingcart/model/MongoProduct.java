package com.webshoppingcart.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


/** @author Dino Cosic */

/**@deprecated  Klasa Products predstavlja entitet iz baze podataka. Sastoji se od osnovnih atributa koji opisuju proizvod koji se nalazi u bazi*/

@Document(collection="products")
public class MongoProduct {
	
	@Id
	private String id;
	@Field("name")
	private String name;
	@Field("date_created")
	private Date date_created;
	@Field("image")
	private byte[] image;
	
	//public MongoProduct() {}

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDate_created() {
		return date_created;
	}
	public void setDate_created(Date date_created) {
		this.date_created = date_created;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	

}
