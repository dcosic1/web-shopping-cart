package com.webshoppingcart.model;


import java.util.ArrayList;
import java.util.List;

public class Table {
    String name;
    List<Column> columns;
    List<PrimaryKey> keys;
    List<Indeks> indeksi;

    public Table(String name) {
        this.name = name;
        columns = new ArrayList<Column>();
        keys = new ArrayList<PrimaryKey>();
        indeksi = new ArrayList<Indeks>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public void addColumn(Column column) {
        columns.add(column);
    }

    public List<PrimaryKey> getPrimaryKeys() {
        return keys;
    }

    public void addPrimaryKey(PrimaryKey key) {
        keys.add(key);
    }

    public List<Indeks> getIndeksi() {
        return indeksi;
    }

    public void addIndeks(Indeks ind) {
        indeksi.add(ind);
    }

}
