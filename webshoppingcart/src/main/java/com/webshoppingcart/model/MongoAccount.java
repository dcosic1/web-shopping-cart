
package com.webshoppingcart.model;



import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Document(collection="accounts")
public class MongoAccount {
	
	@Id
	private String id;
	@Field("username")
	private String username;
	@Field("userRole")
	private String userRole;
	@Field("active")
	private boolean active;
	@Field("password")
	private String password;
	

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String name) {
		this.username=name;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String pass) {
		this.password = pass;
	}


	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public String getUserRole() {
		return userRole;
	}
	
	public void setUserRole(String role) {
		this.userRole=role;
	}
	
	

}
