package com.webshoppingcart.model;


public class Procedures {
    String name;
    short type;

    public Procedures(String name, short type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public String getStringType() {
        switch(type) {
            case 1:
                return "Unknown result.";
            case 2:
                return "No result.";
            case 3:
                return "Result.";
            default:
                return "";
        }
    }

}