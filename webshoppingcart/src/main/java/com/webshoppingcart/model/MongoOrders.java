package com.webshoppingcart.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="orders")
public class MongoOrders {
	@Id
	private Integer id;

	@Field("amount")
	private double amount;

	@Field("customer_name")
	private String customer_name;
	
	@Field("customer_adress")
	private String customer_adress;
	
	@Field("customer_email")
	private String customer_email;
	
	@Field("customer_phone")
	private String customer_phone;

	@Field("order_date")
	private Date order_date;
	
	@Field( "order_num")
	private Integer order_num;

	

	public MongoOrders() {
	}

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customer_name;
	}

	public void setCustomerName(String customer_name) {
		this.customer_name = customer_name;
	}
	
	public String getCustomerEmail() {
		return customer_email;
	}

	public void setCustomerEmail(String customer_email) {
		this.customer_email = customer_email;
	}

	public String getCustomerAdress() {
		return customer_adress;
	}

	public void setCustomerAdress(String customer_adress) {
		this.customer_adress = customer_adress;
	}
	
	public String getCustomerPhone() {
		return customer_phone;
	}

	public void setCustomerPhone(String customer_phone) {
		this.customer_phone = customer_phone;
	}
	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDateOrder() {
		return order_date;
	}

	public void setDateOrder(Date date_created) {
		this.order_date = date_created;
	}
	
	public Integer getOrderNum() {
		return order_num;
	}

	public void setOrderNum(Integer id) {
		this.order_num = id;
	}
}
