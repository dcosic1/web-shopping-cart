package com.webshoppingcart.model;


import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(name = "ACCOUNTS")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_Sequence")
	@SequenceGenerator(name = "account_Sequence", sequenceName = "ACCOUNT_SEQ")
	private Integer id;

	@Column(name = "userName")
	private String userName;

	@Column(name = "userRole")
	private String userRole;

	@Column (name= "active")
	private boolean active;
	
	@Column(name = "password")
	private String password;


	public Account() {
	}

	// getters/setters
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String name) {
		this.userName = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String pass) {
		this.password = pass;
	}


	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public String getUserRole() {
		return userRole;
	}
	
	public void setUserRole(String role) {
		this.userRole=role;
	}
	
}
