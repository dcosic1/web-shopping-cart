package com.webshoppingcart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.webshoppingcart.model.MongoProduct;
/** @author Dino Cosic */

/**@deprecated  Interface koji sluzi za operacije koje se izvrsavaju nad bazom podataka*/
public interface MongoProductRepository extends MongoRepository<MongoProduct, String> {

}
