package com.webshoppingcart.repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.webshoppingcart.model.MongoAccount;

public interface MongoAccountRepository extends MongoRepository <MongoAccount, String> {

}
