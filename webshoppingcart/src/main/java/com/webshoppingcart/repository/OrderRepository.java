package com.webshoppingcart.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.webshoppingcart.model.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Integer>  {

}
