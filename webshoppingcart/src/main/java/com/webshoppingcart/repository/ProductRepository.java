package com.webshoppingcart.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.webshoppingcart.model.Product;;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {
	
}
