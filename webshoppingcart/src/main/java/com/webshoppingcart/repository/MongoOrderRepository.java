package com.webshoppingcart.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.webshoppingcart.model.MongoOrders;



public interface MongoOrderRepository extends MongoRepository<MongoOrders, String> {

}
