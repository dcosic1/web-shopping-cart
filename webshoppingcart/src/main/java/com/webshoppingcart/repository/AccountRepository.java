
package com.webshoppingcart.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.webshoppingcart.model.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {
	
}

