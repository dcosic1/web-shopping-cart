
package com.webshoppingcart.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webshoppingcart.model.MongoAccount;
import com.webshoppingcart.repository.MongoAccountRepository;

@Service
public class MongoAccountService {
	
	@Autowired
	private MongoAccountRepository mongoAccountRepository;
	
	
	public Iterable<MongoAccount> getAccounts() {
		return mongoAccountRepository.findAll();
	}
	
	public Optional<MongoAccount> getAccountById(String id) {
		return mongoAccountRepository.findById(id);
	}
}
