package com.webshoppingcart.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webshoppingcart.model.Order;
import com.webshoppingcart.repository.OrderRepository;

@Service
public class OrderServiceImpl implements OrderService{

	@Autowired
	private OrderRepository orderRepository;

	public Iterable<Order> getOrders() {
		return orderRepository.findAll();
	}

	@Override
	public Optional<Order> getOrderById(Integer id) {
		return orderRepository.findById(id);
	}
}
