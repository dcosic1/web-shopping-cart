package com.webshoppingcart.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.webshoppingcart.model.Product;

@Service
public interface ProductService {

	public Iterable<Product> getProducts();
	public Optional<Product> getProductById(Integer id);
}
