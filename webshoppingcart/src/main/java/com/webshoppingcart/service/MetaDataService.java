package com.webshoppingcart.service;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.webshoppingcart.model.Indeks;
import com.webshoppingcart.model.Column;
import com.webshoppingcart.model.PrimaryKey;
import com.webshoppingcart.model.Procedures;
import com.webshoppingcart.model.Table;

import org.springframework.stereotype.Service;

@Service
public class MetaDataService {
    org.hibernate.engine.spi.SessionImplementor sessionImplementor;
    DatabaseMetaData metaData;

    @PersistenceContext
    private EntityManager em;

    private void initializeMetaData() {
        sessionImplementor = (org.hibernate.engine.spi.SessionImplementor) em.getDelegate();
        try {
            metaData = sessionImplementor.connection().getMetaData();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<Table> getTables() {
        initializeMetaData();
        String[] types = {"TABLE"};
        try {
            List<Table> listTabels = new ArrayList<>();
            ResultSet tabels = metaData.getTables(null, "BP21", "%", types);
            while(tabels.next()) {
                Table tabel = new Table(tabels.getString(3));
                
                ResultSet columns = metaData.getColumns(null, "%", tabels.getString(3), "%");
                while(columns.next()) {
                    Column column = new Column(columns.getString(4), columns.getString(6), columns.getString(7));
                    tabel.addColumn(column);;
                }
                columns.close();

                ResultSet keys = metaData.getPrimaryKeys(null, "BP21", tabel.getName());
                while(keys.next()) {
                    tabel.addPrimaryKey(new PrimaryKey(keys.getString(4), keys.getString(6)));
                }
                keys.close();

                ResultSet indeksi = metaData.getIndexInfo(null, "BP21", tabel.getName(), false, true);
                while(indeksi.next()) {
                    tabel.addIndeks(new Indeks(indeksi.getString(6), indeksi.getShort(7), indeksi.getString(9)));
                }
                indeksi.close();

                listTabels.add(tabel);
            }
            tabels.close();
            return listTabels;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<Procedures> getProcedure() {
        try {
            ResultSet procedure = metaData.getProcedures(null, "BP21", "%");
            List<Procedures> listaProcedures = new ArrayList<>();
            while(procedure.next()) {
                listaProcedures.add(new Procedures(procedure.getString(3), procedure.getShort(8)));
            }
            return listaProcedures;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public List<Procedures> getFunctions() {
        try {
            ResultSet functions = metaData.getFunctions(null, "BP21", "%");
            List<Procedures> listFunctions = new ArrayList<>();
            while(functions.next()) {
                listFunctions.add(new Procedures(functions.getString(3), functions.getShort(8)));
            }
            return listFunctions;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<String> getTrigers() {
        try {
            ResultSet trigers = metaData.getTables(null, "BP21", "%", new String[] {"TRIGGER"});
            List<String> listTrigers = new ArrayList<>();
            while(trigers.next()) {
                listTrigers.add(trigers.getString(3));
            }
            return listTrigers;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}


