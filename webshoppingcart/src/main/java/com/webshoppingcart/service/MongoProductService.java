package com.webshoppingcart.service;

import static org.hamcrest.CoreMatchers.nullValue;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webshoppingcart.model.MongoProduct;
import com.webshoppingcart.model.Product;
import com.webshoppingcart.repository.MongoProductRepository;
import com.webshoppingcart.repository.ProductRepository;

/** @author Dino Cosic */

/**
 * @deprecated Klasa MongoProductService implementira metode koje se odnose na
 *             dohvatanje podataka iz MongoDb-a na veoma jednostavan nacin
 */
@Service
public class MongoProductService {

	@Autowired
	private MongoProductRepository mongoProductRepository;

	@Autowired
	ProductRepository productRepository;

	/** @param Ova funkcija vraca sve proizvode iz baze */
	public Iterable<MongoProduct> getProducts() {
		return mongoProductRepository.findAll();
	}

	/** @param Ova funkcija vraca proizvod iz baze sa odgovarajucim id-om */
	public Optional<MongoProduct> getProductById(String id) {
		Optional<Product> oProduct = null;
		Optional<MongoProduct> mProduct = null;
		Integer oId = Integer.parseInt(id);
		oProduct = productRepository.findById(oId);
		System.out.println("ORACLE za isti ID:" + oId + " : " + oProduct);
		mProduct = mongoProductRepository.findById(id);
		return mProduct;
	}
}
