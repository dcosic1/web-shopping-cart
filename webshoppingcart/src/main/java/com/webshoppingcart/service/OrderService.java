

package com.webshoppingcart.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.webshoppingcart.model.Order;

@Service
public interface OrderService {

	public Iterable<Order> getOrders();
	public Optional<Order> getOrderById(Integer id);
}
