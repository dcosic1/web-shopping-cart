package com.webshoppingcart.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webshoppingcart.model.Product;
import com.webshoppingcart.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	/**@param Ova funkcija vraca sve proizvode iz baze */
	public Iterable<Product> getProducts() {
		return productRepository.findAll();
	}
	/**@param Ova funkcija vraca proizvod iz baze sa odgovarajucim id-om */
	@Override
	public Optional<Product> getProductById(Integer id) {
		return productRepository.findById(id);
	}

}
