package com.webshoppingcart.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.webshoppingcart.model.Account;

@Service
public interface AccountService {
	public Iterable<Account> getAccounts();
	public Optional<Account> getAccountById(Integer id);
}




