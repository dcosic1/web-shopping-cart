package com.webshoppingcart.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webshoppingcart.model.MongoOrders;
import com.webshoppingcart.repository.MongoOrderRepository;

@Service
public class MongoOrderService {

	@Autowired
	private MongoOrderRepository mongoOrderRepository;
	public Iterable<MongoOrders> getOrders() {
		return mongoOrderRepository.findAll();
	}
	
	public Optional<MongoOrders> getOrderById(String id) {
		return mongoOrderRepository.findById(id);
	}
}
