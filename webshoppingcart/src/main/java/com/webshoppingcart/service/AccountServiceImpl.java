package com.webshoppingcart.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webshoppingcart.model.Account;
import com.webshoppingcart.repository.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService {
	@Autowired
	private AccountRepository accountRepository;

	public Iterable<Account> getAccounts() {
		return accountRepository.findAll();
	}

	@Override
	public Optional<Account> getAccountById(Integer id) {
		return accountRepository.findById(id);
	}
}




