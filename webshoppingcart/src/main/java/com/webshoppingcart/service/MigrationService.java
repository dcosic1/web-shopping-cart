package com.webshoppingcart.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.webshoppingcart.connection.Database;
import com.webshoppingcart.connection.DatabasteOracle;
import com.webshoppingcart.model.Indeks;
import com.webshoppingcart.model.Column;
import com.webshoppingcart.model.Procedures;
import com.webshoppingcart.model.Table;

@Service
public class  MigrationService {
	public Connection konekcijaNaMySQL() {
		Connection con = null;
		try {
			con = Database.getConnection();
			Statement stmt = con.createStatement();
			String sql = "use bazemigracija;";
			stmt.executeQuery(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;

	}

	public Connection konekcijaNaOracle() {
		Connection konekcija = null;
		try {
			konekcija = DatabasteOracle.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return konekcija;

	}

	public void migracijaTabela(List<Table> tabele, Connection con) throws Exception {

		try {
			Statement stmt2 = con.createStatement();

			for (Table tabela1 : tabele) {
				String postoji = "drop table if exists " + tabela1.getName();
				stmt2.execute(postoji);

				String sql2 = "create table " + tabela1.getName() + " (";
				for (Column k : tabela1.getColumns()) {
					String tip = k.getType();
					if (tip.equals("NUMBER"))
						tip = "INT";
					if (tip.equals("VARCHAR2"))
						tip = "VARCHAR";
					if (tip.equals("TIMESTAMP(6)"))
						tip = "VARCHAR";
					if (tip.equals("BLOB"))
						tip = "VARCHAR"; 
					sql2 += " " + k.getName() + " " + tip + " (" + (Integer.parseInt(k.getSize()) + 20) + ") ";
					if (tabela1.getPrimaryKeys().size() != 0
							&& k.getName().equals(tabela1.getPrimaryKeys().get(0).getColumn())) {
						sql2 += " primary key ";
					}
					sql2 += ", ";
				}
				sql2 = sql2.substring(0, sql2.length() - 2);
				sql2 += " );";
				System.out.println(sql2);
				stmt2.execute(sql2);

			}
			System.out.println("Kraj petlje");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void migracijaPodataka(List<Table> tabele, Connection con, Connection konekcija) throws Exception {
		try {
			Statement stmt3 = konekcija.createStatement();
			Statement stmt2 = con.createStatement();
			ResultSet podaci = null;

			for (Table tabela1 : tabele) {
				
				
				String dohvatiPodatke = "select * from " + tabela1.getName();
				podaci = stmt3.executeQuery(dohvatiPodatke);

				ArrayList<String> getaj = new ArrayList<String>();
				for (Column k : tabela1.getColumns()) {
					String tip = k.getType();
					if (tip.equals("NUMBER"))
						getaj.add("getInt");
					if (tip.equals("VARCHAR2"))
						getaj.add("getString");
					if (tip.equals("TIMESTAMP(6)"))
						getaj.add("getDate");
					if (tip.equals("BLOB"))
						getaj.add("getBlob");

				}

				while (podaci.next()) {
					
					String sql3 = "insert into " + tabela1.getName() + " VALUES (";
					for (int i = 1; i < getaj.size() + 1; i++) {
						if (getaj.get(i - 1).equals("getInt"))
							sql3 += podaci.getInt(i);
						if (getaj.get(i - 1).equals("getString"))
							sql3 += ("'" + podaci.getString(i) + "'");
						if (getaj.get(i - 1).equals("getDate"))
							sql3 += ("'" + podaci.getString(i) + "'");
						if (getaj.get(i - 1).equals("getBlob"))
							sql3 += "'BlobFIle'";

						sql3 += ",";

					}
				
					
					sql3 = sql3.substring(0, sql3.length() - 1);
					sql3 += ");";
					System.out.println(sql3);
					stmt2.execute(sql3);
				}

			}

			System.out.println("Kraj petlje za podatke");

		} catch (Exception e) {
			throw(e);
		}

	}

	public void migracijaTrigera(Connection con, Connection konekcija) {
		try {
			Statement stmt3 = konekcija.createStatement();
			Statement stmt2 = con.createStatement();
			String novi = "SELECT * FROM USER_TRIGGERS";
			ResultSet rsnovi = null;
			rsnovi = stmt3.executeQuery(novi);

			while (rsnovi.next()) {
				Statement stmt5 = con.createStatement();
				String postoji = "DROP TRIGGER IF EXISTS " + rsnovi.getString("TRIGGER_NAME");
				stmt2.execute(postoji);
				String sql4 = "CREATE TRIGGER " + rsnovi.getString("DESCRIPTION").toUpperCase() + "BEGIN \n";
				String body = rsnovi.getString("TRIGGER_BODY").toUpperCase();
				body = body.replaceAll("BEGIN", "");
				sql4 += body;
				sql4 = sql4.substring(0, sql4.length() - 1);
				sql4 += ";";
				String brisi = ":";
				sql4 = sql4.replaceAll(brisi, "");
				sql4 = sql4.replaceAll("NUMBER", "INT");
				sql4 = sql4.replaceAll("RAISE.*",
						"SIGNAL SQLSTATE '45000'\n" + "SET MESSAGE_TEXT = 'Order No not found in orders table';\r\n");
				System.out.println(sql4);
				stmt5.execute(sql4);

			}

		} catch (Exception e) {
			
					e.printStackTrace();

		}
	}

	public void migracijaProcedura(List<Procedures> procedure, Connection con, Connection konekcija) {
		try {
			ResultSet rsFun = null;
			Statement stmt3 = konekcija.createStatement();
			Statement stmt2 = con.createStatement();
			for (Procedures p : procedure) {
				String bodyFunkcije = "";
				bodyFunkcije = "select text from user_source\r\n" + "where name = '" + p.getName() + "' \norder by line";
				bodyFunkcije = bodyFunkcije.substring(0, bodyFunkcije.length());
				rsFun = stmt3.executeQuery(bodyFunkcije);
				String kodFunkcije = "";
				while (rsFun.next()) {
					kodFunkcije += rsFun.getString(1);
				}
				System.out.println(kodFunkcije);
				

			}
		} catch (Exception e) {
			
			e.printStackTrace();

		}
	}

	public void migracijaIndeksa(List<Table> tabele, Connection con) {
		try {
			Statement stmt2 = con.createStatement();
			List<Indeks> indeksi = new ArrayList<Indeks>();
			String sqlI = "";
			for (Table tabela1 : tabele) {
				indeksi = tabela1.getIndeksi();
				for (Indeks i : indeksi) {
					String nazivIndeksa = i.getName();
					String naziv_tabele = tabela1.getName();
					String naziv_kolone = i.getColumn();
					sqlI = "CREATE INDEX " + nazivIndeksa + " ON " + naziv_tabele + " (" + naziv_kolone + ");";
					// stmt2.execute(sqlI);
				}
				System.out.println(sqlI);
			}
		} catch (Exception e) {
			
			e.printStackTrace();

		}
	}
}
