package com.webshppingcart.servlets;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/uploadToDB")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
		maxFileSize = 1024 * 1024 * 10, // 10MB
		maxRequestSize = 1024 * 1024 * 50) // 50MB

public class UploadToDBServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/jsps/uploadToDB.jsp");

		dispatcher.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection conn = null;
		try {
			// Connection to Database
			// (See more in JDBC Tutorial).

			String hostName = "ora.db.lab.ri.etf.unsa.ba";
			String sid = "ETFLAB";
			String userName = "BP21";
			String password = "wP78eev7";

			String connectionURL = "jdbc:oracle:thin:@" + hostName + ":1521:" + sid;

			conn = DriverManager.getConnection(connectionURL, userName, password);
			conn.setAutoCommit(false);
			// ---------------------------------------------------------------------

			String description = request.getParameter("description");

			// Part list (multi files).
			for (Part part : request.getParts()) {
				String fileName = extractFileName(part);
				if (fileName != null && fileName.length() > 0) {
					// File data
					InputStream is = part.getInputStream();
					// Write to file
					this.writeToDB(conn, fileName, is, description);
				}
			}
			conn.commit();

			// Upload successfully!.
			response.sendRedirect(request.getContextPath() + "/uploadToDBResults");
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("errorMessage", "Error: " + e.getMessage());
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/jsps/uploadToDB.jsp");
			dispatcher.forward(request, response);
		} finally {
			this.closeQuietly(conn);
		}
	}

	private String extractFileName(Part part) {
		// form-data; name="file"; filename="C:\file1.zip"
		// form-data; name="file"; filename="C:\Note\file2.zip"
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				// C:\file1.zip
				// C:\Note\file2.zip
				String clientFileName = s.substring(s.indexOf("=") + 2, s.length() - 1);
				clientFileName = clientFileName.replace("\\", "/");
				int i = clientFileName.lastIndexOf('/');
				// file1.zip
				// file2.zip
				return clientFileName.substring(i + 1);
			}
		}
		return null;
	}

	private Long getMaxAttachmentId(Connection conn) throws SQLException {
		String sql = "Select max(a.id) from Attachment a";
		PreparedStatement pstm = conn.prepareStatement(sql);
		ResultSet rs = pstm.executeQuery();
		if (rs.next()) {
			long max = rs.getLong(1);
			return max;
		}
		return 0L;
	}

	public void writeToDB(Connection conn, String fileName, InputStream is, String description) throws SQLException {

		String sql = "Insert into Attachment(ID,FILE_NAME,FILE_DATA,DESCRIPTION) " //
				+ " values (?,?,?,?) ";
		String sql2 = "Insert into Products(ID,DATE_CREATED,IMAGE,NAME,PRICE) " //
				+ " values (?,?,?,?, ?) ";
		PreparedStatement pstm = conn.prepareStatement(sql);
		PreparedStatement pstm2 = conn.prepareStatement(sql2);
		
		Long id = this.getMaxAttachmentId(conn) + 1;
		pstm.setLong(1, id);
		pstm.setString(2, fileName);
		pstm.setBlob(3, is);
		pstm.setString(4, description);
		pstm.executeUpdate();
		
		
		Long id2 = this.getMaxAttachmentId(conn);
		pstm2.setLong(1, id2);
		pstm2.setString(2, null);
		pstm2.setString(3, null);
		pstm2.setString(4, description);
		pstm2.setString(5, null);		
		pstm2.executeUpdate();
		
		
	}

	private void closeQuietly(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
		}
	}

}
