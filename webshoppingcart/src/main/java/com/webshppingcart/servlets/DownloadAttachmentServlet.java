package com.webshppingcart.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
 
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.webshoppingcart.model.Attachment;



@WebServlet("/downloadAttachment")
public class DownloadAttachmentServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection conn = null;
		try {
			// Get Database Connection.

			String hostName = "ora.db.lab.ri.etf.unsa.ba";
			String sid = "ETFLAB";
			String userName = "BP21";
			String password = "wP78eev7";

			String connectionURL = "jdbc:oracle:thin:@" + hostName + ":1521:" + sid;
			conn = DriverManager.getConnection(connectionURL, userName, password);
			Long id = null;
			
			try {
				id = Long.parseLong(request.getParameter("id"));
			} catch (Exception e) {

			}
			Attachment attachment = getAttachmentFromDB(conn, id);

			if (attachment == null) {
				// No record found.
				response.getWriter().write("No data found");
				return;
			}

			// file1.zip, file2.zip
			String fileName = attachment.getFileName();
			System.out.println("File Name: " + fileName);

			// abc.txt => text/plain
			// abc.zip => application/zip
			// abc.pdf => application/pdf
			String contentType = this.getServletContext().getMimeType(fileName);
			System.out.println("Content Type: " + contentType);

			response.setHeader("Content-Type", contentType);

			response.setHeader("Content-Length", String.valueOf(attachment.getFileData().length()));

			response.setHeader("Content-Disposition", "inline; filename=\"" + attachment.getFileName() + "\"");

			// For big BLOB data.
			Blob fileData = attachment.getFileData();
			InputStream is = fileData.getBinaryStream();

			byte[] bytes = new byte[1024];
			int bytesRead;

			while ((bytesRead = is.read(bytes)) != -1) {
				// Write image data to Response.
				response.getOutputStream().write(bytes, 0, bytesRead);
			}
			is.close();

		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			this.closeQuietly(conn);
		}
	}

	public Attachment getAttachmentFromDB(Connection conn, Long id) throws SQLException {
		String sql = "Select a.Id,a.FILE_NAME,a.FILE_DATA,a.DESCRIPTION "//
				+ " from Attachment a where a.id = ?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setLong(1, id);
		ResultSet rs = pstm.executeQuery();
		if (rs.next()) {
			String fileName = rs.getString("FILE_NAME");
			Blob fileData = rs.getBlob("FILE_DATA");
			String description = rs.getString("DESCRIPTION");
			return new Attachment(id, fileName, fileData, description);
		}
		return null;
	}

	private void closeQuietly(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
		}
	}
}
